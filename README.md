# README #
This app is capable of sorting fotos according to events, which have to be specified src/main/resources 
The architecture bases on SpringBatch - mainly for eduational reasons - not sure, if I would choose it again for this solution 
# Process #
## provide pics ##
- move pics to sort under "PhotosSortMe"
- copy the pics to folder "now" 
    - this step is necessary for reconciliation reasons and for easy rerun of the batch if necessary as you keep the orignal

## prepare pics ##
this step is basically to flatten all files into one folder and generate an allpics.csv file
yes it could be done easier, but SpringBatch is easily capable of processing a file line by line
- change CURRENT_FOLDER to the name of your current pics folder you've copied to "now"
- run FilePreparation.java
- check reconciliation at the end - minor differences might stem from not correctly counted folders
- check folder "little_movies" - this might be deleted, as Apple stores "live photo" information as a .mov file - most of the time, I'm not interested in those ones
    - delete littl_movies?

## sort pics ##
- run FotoSorterBatch
- check output
    - minor differences might stem from not correctly counted folders

BUG: EventList doesnt recognise eventy per day - only if fotos are between two dates