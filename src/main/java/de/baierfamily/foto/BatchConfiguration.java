package de.baierfamily.foto;

        import org.springframework.batch.core.Job;
        import org.springframework.batch.core.Step;
        import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
        import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
        import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
        import org.springframework.batch.item.file.FlatFileItemReader;
        import org.springframework.batch.item.file.FlatFileItemWriter;
        import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
        import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
        import org.springframework.batch.item.file.mapping.FieldSetMapper;
        import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
        import org.springframework.batch.item.file.transform.DelimitedLineAggregator;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.beans.factory.annotation.Value;
        import org.springframework.context.annotation.Bean;
        import org.springframework.context.annotation.Configuration;
        import org.springframework.core.convert.ConversionService;
        import org.springframework.core.convert.converter.Converter;
        import org.springframework.core.convert.support.DefaultConversionService;
        import org.springframework.core.io.ClassPathResource;
        import org.springframework.core.io.FileSystemResource;
        import org.springframework.core.io.FileUrlResource;

        import java.net.MalformedURLException;
        import java.time.LocalDate;
        import java.time.format.DateTimeFormatter;

@Configuration
@EnableBatchProcessing
public class BatchConfiguration {

  @Autowired
  public JobBuilderFactory jobBuilderFactory;

  @Autowired
  public StepBuilderFactory stepBuilderFactory;

  @Value("${events.file.input}")
  private String eventFileInput;

  @Value("${fotos.file.input}")
  private String fotoFileInput;

  @Bean
  public FlatFileItemReader<Event> eventItemReader() throws MalformedURLException {
    return new FlatFileItemReaderBuilder<Event>().name("EvenItemReader")
            .resource(new ClassPathResource(eventFileInput))
            .delimited().delimiter(",")
            .names(new String[] { "name", "begin", "end" })
            .fieldSetMapper(new BeanWrapperFieldSetMapper<Event>() {{
              setTargetType(Event.class);
              setConversionService(stringLocalDateConversion());
            }})
            .build();
  }

  @Bean
  public FlatFileItemReader<Foto> fotoItemReader() throws MalformedURLException {
    return new FlatFileItemReaderBuilder<Foto>().name("FotoItemReader")
            .resource(new FileUrlResource(fotoFileInput))
            .delimited().delimiter(",")
            .names(new String[] { "fileName"})
            .fieldSetMapper(new BeanWrapperFieldSetMapper<Foto>() {{
              setTargetType(Foto.class);
            }})
            .build();
  }

  @Bean
  public ConversionService stringLocalDateConversion() {
    DefaultConversionService testConversionService = new DefaultConversionService();
    DefaultConversionService.addDefaultConverters(testConversionService);
    testConversionService.addConverter(new Converter<String, LocalDate>() {
      @Override
      public LocalDate convert(String text) {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        return LocalDate.parse(text, formatter);
      }
    });

    return testConversionService;
  }

  @Bean
  public FieldSetMapper<Event> testClassRowMapper(ConversionService stringLocalDateConversion) {
    BeanWrapperFieldSetMapper<Event> mapper = new BeanWrapperFieldSetMapper<>();
    mapper.setConversionService(stringLocalDateConversion);
    mapper.setTargetType(Event.class);
    return mapper;
  }

  @Bean
  public EventProcessor eventProcessor() {
    return new EventProcessor();
  }

  @Bean
  public FotoProcessor fotoProcessor() {
    return new FotoProcessor();
  }

  @Bean
  public Job movePicsJob(JobCompletionNotificationListener listener, Step stepReadEvents_1, Step stepCopyFotos_2) {
    return jobBuilderFactory.get("moveFotoJob")
            .listener(new ImageCopyBatchListener())
            .flow(stepReadEvents_1)
            .next(stepCopyFotos_2)
            .end()
            .build();
  }

  @Bean
  public FlatFileItemWriter<Event> eventItemWriter() {
    BeanWrapperFieldExtractor<Event> fieldExtractor = new BeanWrapperFieldExtractor<>();
    fieldExtractor.setNames(new String[] {"name", "begin", "end"});
    fieldExtractor.afterPropertiesSet();

    DelimitedLineAggregator<Event> lineAggregator = new DelimitedLineAggregator<>();
    lineAggregator.setDelimiter(",");
    lineAggregator.setFieldExtractor(fieldExtractor);

    FlatFileItemWriter<Event> flatFileItemWriter = new FlatFileItemWriter<>();
    flatFileItemWriter.setName("EventItemWriter");
    flatFileItemWriter.setResource(new FileSystemResource("target/test-outputs/Events.txt"));
    flatFileItemWriter.setLineAggregator(lineAggregator);

    return flatFileItemWriter;
  }

  @Bean
  public FlatFileItemWriter<Foto> fotoItemWriter() {
    BeanWrapperFieldExtractor<Foto> fieldExtractor = new BeanWrapperFieldExtractor<>();
    fieldExtractor.setNames(new String[] {"fileName", "destination"});
    fieldExtractor.afterPropertiesSet();

    DelimitedLineAggregator<Foto> lineAggregator = new DelimitedLineAggregator<>();
    lineAggregator.setDelimiter(",");
    lineAggregator.setFieldExtractor(fieldExtractor);

    FlatFileItemWriter<Foto> flatFileItemWriter = new FlatFileItemWriter<>();
    flatFileItemWriter.setName("EventItemWriter");
    flatFileItemWriter.setResource(new FileSystemResource("target/test-outputs/FotosTarget.txt"));
    flatFileItemWriter.setLineAggregator(lineAggregator);

    return flatFileItemWriter;
  }


  @Bean
  public Step stepReadEvents_1(FlatFileItemWriter<Event> eventItemWriter) throws MalformedURLException {
    return stepBuilderFactory.get("stepReadEvents_1")
            .<Event, Event> chunk(10)
            .reader(eventItemReader())
            .processor(eventProcessor())
            .writer(eventItemWriter)
            .build();
  }

  @Bean
  public Step stepCopyFotos_2(FlatFileItemWriter<Foto> fotoItemWriter) throws MalformedURLException {
    return stepBuilderFactory.get("stepCopyFotos_2")
            .<Foto, Foto> chunk(10)
            .reader(fotoItemReader())
            .processor(fotoProcessor())
            .writer(fotoItemWriter)
            .build();
  }

}