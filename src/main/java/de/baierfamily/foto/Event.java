package de.baierfamily.foto;

import lombok.Data;

import java.time.LocalDate;


@Data
public class Event {
  private String name;
  private LocalDate begin;
  private LocalDate end;
}