package de.baierfamily.foto;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAmount;
import java.time.temporal.TemporalUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class EventList {

  private static final List<Event> ALL_EVENTS = new ArrayList<>();

  public static void addEvent(Event event) {
    ALL_EVENTS.add(event);
  }

  public static Event findEvent(LocalDate picDate) {
    if(picDate==null){
      return null;
    }
    List<Event> filteredEvent = ALL_EVENTS.stream().filter((event) -> {
      LocalDate beginPrep= event.getBegin().minusDays(1);
      LocalDate endPrep= event.getEnd().plusDays(1);
      return beginPrep.isBefore(picDate) && endPrep.isAfter(picDate);
    }).collect(Collectors.toList());

    if (filteredEvent.size() > 0) {
      return filteredEvent.get(0);
    }
    else return null;
  }

  public static int eventCount() {
    return ALL_EVENTS.size();
  }

}
