package de.baierfamily.foto;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.batch.item.ItemProcessor;

public class EventProcessor implements ItemProcessor<Event, Event> {

  private static final Logger LOGGER = LoggerFactory.getLogger(EventProcessor.class);

  @Override
  public Event process(final Event event) throws Exception {
    String name = event.getName().toUpperCase();
    String begin = event.getBegin().toString();
    String end = event.getEnd().toString();

    LOGGER.info("Reading event ( {} )", event);
    EventList.addEvent(event);
    return event;
  }

}