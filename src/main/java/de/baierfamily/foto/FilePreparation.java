package de.baierfamily.foto;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.io.filefilter.FileFileFilter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static org.apache.commons.io.FileUtils.listFiles;

@Slf4j
/**
 * moves all pics from PHOTOS_TO_PREPARE to
 * - now/ready -> ready to be sorted
 * - now/duplicats -> pic already present in now/ready store for reconciliation reasons (not yet implemented)
 */
public class FilePreparation {

  public static final String CURRENT_FOLDER="2017_PetrasIphone";
  public static final String PHOTOS_TO_PREPARE = "./PhotosSortMe/now/"+CURRENT_FOLDER;
  public static final String ORIGINAL_PHOTOS_TO_PREPARE = "./PhotosSortMe/"+CURRENT_FOLDER;
  public static final String PHOTOS_SORT_ME_NOW_READY = "./PhotosSortMe/now/ready";
  public static final int MIN_SIZE_FOR_MOVIES = 5000000; // 5 MEGS

  public static void main(String[] args) throws IOException {

    Collection<File> originalForReconciliationFilesToMove = listFiles(new File(ORIGINAL_PHOTOS_TO_PREPARE), FileFileFilter.FILE, DirectoryFileFilter.DIRECTORY);
    Collection<File> sourceFilesToMove = listFiles(new File(PHOTOS_TO_PREPARE), FileFileFilter.FILE, DirectoryFileFilter.DIRECTORY);
    int picsToMove = sourceFilesToMove.size();
    log.info("number of files before action:" + picsToMove);
    deleteSubDirsIfEmpty(PHOTOS_TO_PREPARE);
    File targetDir = mkDirIfNotExists(PHOTOS_SORT_ME_NOW_READY);
    File duplicatesDir = mkDirIfNotExists("./PhotosSortMe/now/duplicates");

    sourceFilesToMove.forEach(f -> {
      try {
        FileUtils.moveFileToDirectory(f, targetDir, true);
      } catch (IOException e) {
        try {
          log.info("moving duplicate file to duplicates: ");
          FileUtils.moveFileToDirectory(f, duplicatesDir, true);
        } catch (IOException ioException) {
          log.error("cannot move file to duplicates", e);
        }
      }
    });
    // finish stuff
    reconciliate(originalForReconciliationFilesToMove, sourceFilesToMove, targetDir, duplicatesDir);
    deleteSubDirsIfEmpty(PHOTOS_TO_PREPARE);
    renameSourceFolder(PHOTOS_TO_PREPARE);
    moveLittleMoviesToQuarantine(PHOTOS_SORT_ME_NOW_READY);
    createAllPicsCSVFile(PHOTOS_SORT_ME_NOW_READY);
  }

  private static File mkDirIfNotExists(String s) {

    File dir = new File(s);
    if(!dir.exists()){
      dir.mkdir();
    }

    return dir;
  }

  private static void reconciliate(Collection<File> originalForReconciliationFilesToMove, Collection<File> sourceFilesToMove, File targetDir, File duplicatesDir) {
    int originalFilesToMoveCount = originalForReconciliationFilesToMove.size();
    int sourceFilesToMoveCount = sourceFilesToMove.size();
    int movedToTargetDirCount=FileUtils.listFiles(targetDir, null, true).size();
    int movedToDuplicatesDirCount=FileUtils.listFiles(duplicatesDir, null, true).size();


    log.info("==============================");
    log.info("numbers should be identical:");
    log.info("original files to move: # "+ originalFilesToMoveCount);
    log.info("source   files to move: # "+ sourceFilesToMoveCount);
    log.info("==============================");
    log.info("files moved to target : # "+ movedToTargetDirCount);
    log.info("files moved to duplic : # "+ movedToDuplicatesDirCount);
    log.info("==============================");
    int moveDelta = sourceFilesToMoveCount - movedToTargetDirCount - movedToDuplicatesDirCount;
    log.info("move delta: # " + moveDelta);
    if(moveDelta>0){
      log.info("==============================");
      log.error("+++++++++ FAILURE +++++++++++");
      log.error("gap detected! Check folders, something went wrong as moveDelta is not 0");
      log.info("giving up");
      System.exit(-1);
    }
  }

  private static void deleteSubDirsIfEmpty(String rootdir) {
    Collection<File> files = FileUtils.listFilesAndDirs(mkDirIfNotExists(rootdir), DirectoryFileFilter.DIRECTORY, DirectoryFileFilter.DIRECTORY);
    files.stream().forEach(f ->{
      if(f.isDirectory()){
        Collection<File> filesInDir = listFiles(f, null, false);
        log.debug(f.getName() + " #files: " + filesInDir.size());
        if(filesInDir.size()==0){
          f.delete();
        }
      }else{
        log.debug("found file: " + f.getName());
      }
    } );
    Collection<File> filesAfterDelete = FileUtils.listFilesAndDirs(mkDirIfNotExists(rootdir), DirectoryFileFilter.DIRECTORY, DirectoryFileFilter.DIRECTORY);
    List<File> allLeftDirs = filesAfterDelete.stream().filter(File::isDirectory).collect(Collectors.toList());
    log.info("Left Dirs still alive: " +allLeftDirs);
  }


  private static void renameSourceFolder(String photosToPrepare) {
    File sourceDir = new File(photosToPrepare);
    Collection<File> expectedNoChildren = listFiles(sourceDir, FileFileFilter.FILE, DirectoryFileFilter.DIRECTORY);
    String folderSuffix="";
    if(expectedNoChildren.size()==1){ // its only the rootfolder itself left
      folderSuffix="_deleteMe";
    }else{
      folderSuffix="_checkMeIShouldBeEmpty";
    }
    File targetDir = new File(photosToPrepare+folderSuffix);
    sourceDir.renameTo(targetDir);
    log.info("sourceDir rename to " + targetDir.getName());
  }
  private static void createAllPicsCSVFile(String photosSortMeNowReady) throws IOException {
    File sourceDir = new File(photosSortMeNowReady);
    Collection<File> allPics = listFiles(sourceDir, FileFileFilter.FILE, DirectoryFileFilter.DIRECTORY);
    String allPicsCSVName = photosSortMeNowReady + "/allpics.csv";
    File allPicsCSVFile = new File(allPicsCSVName);
    log.info("deleted allPicsCSVFile:"+allPicsCSVFile.delete());
    FileWriter fileWriter = new FileWriter(allPicsCSVName);
    allPics.forEach(f -> {
      try {
        fileWriter.write(f.getName()+"\n");
      } catch (IOException e) {
        log.error("could not write allPics ... giving up", e);
        System.exit(-1);
        try {
          fileWriter.close();
        } catch (IOException ioException) {
          log.error("even closing failed... ",e);
        }
      }
    });
    fileWriter.close();
    log.info("File writing done.");

  }

  private static void moveLittleMoviesToQuarantine(String photosSortMeNowReady) {
    String littleMoviesDir = photosSortMeNowReady + File.separator+"littleMovies";
    File littleMoviesDirFile= new File(littleMoviesDir);
    mkDirIfNotExists(littleMoviesDir);
    Collection<File> allMovies = listFiles(new File(photosSortMeNowReady), new String[]{"MOV","mov"}, true);
    allMovies.forEach(m -> {
      if(m.length() < MIN_SIZE_FOR_MOVIES){
        try {
          FileUtils.moveFileToDirectory(m, littleMoviesDirFile, true);
        } catch (IOException e) {
          log.error("could not move: "+ m.getName(), e);
        }
      }
    });
  }
}
