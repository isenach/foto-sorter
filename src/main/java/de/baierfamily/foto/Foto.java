package de.baierfamily.foto;

import lombok.Data;

@Data
public class Foto {
  private String fileName;
  private String destination;
}
