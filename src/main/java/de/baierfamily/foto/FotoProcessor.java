package de.baierfamily.foto;

import lombok.extern.log4j.Log4j2;
import org.apache.commons.imaging.ImageReadException;
import org.apache.commons.imaging.Imaging;
import org.apache.commons.imaging.common.ImageMetadata;
import org.apache.commons.imaging.formats.jpeg.JpegImageMetadata;
import org.apache.commons.imaging.formats.tiff.TiffField;
import org.apache.commons.imaging.formats.tiff.constants.TiffTagConstants;
import org.apache.commons.imaging.formats.tiff.taginfos.TagInfo;
import org.apache.commons.io.FileUtils;
import org.springframework.batch.item.ItemProcessor;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.Locale;

@Log4j2
public class FotoProcessor implements ItemProcessor<Foto, Foto> {

  public static final String SOURCE_DIR = FilePreparation.PHOTOS_SORT_ME_NOW_READY;
  public static final String TARGET_DIR = "targetFotoRoot";
  public Integer counter = 0;
  public static final LocalDate DEFAULT_ERROR_DATE = LocalDate.of(1900, Month.JANUARY, 1);

  @Override
  public Foto process(final Foto foto) throws Exception {
    counter++;
    File fotoFile = toFile(foto.getFileName());
    LocalDate fotoShootDate = extractFotoShootDate(fotoFile);
    Event event = EventList.findEvent(fotoShootDate);
    String destinationDir = calculateDestinationDir(event, fotoShootDate);
    foto.setDestination(destinationDir);
    copyToDestination(fotoFile, foto.getDestination());
    log.info("processed #" + counter);
    return foto;
  }

  private void copyToDestination(File fotoFile, String destinationPathString) {
    try {
      log.debug(String.format("FotoProcessor: copying now %s to %s ", fotoFile.getName(), destinationPathString));
      FileUtils.copyFileToDirectory(fotoFile, new File(destinationPathString));
    } catch (IOException e) {
      log.error(String.format("failed to copy file %s to destinationDir %s", fotoFile.getAbsolutePath(), destinationPathString));
    }

  }

  private String calculateDestinationDir(Event event, LocalDate fotoShootDate) {
    int year = 0;
    if (event != null) {
      year = event.getBegin().getYear();
      String eventNameDirReady = event.getName().replace(" ", "_");
      return TARGET_DIR + "/" + year + "/" + eventNameDirReady;
    }
    if (fotoShootDate != null) {
      String month = monthInTwoDigitNumbers(fotoShootDate) + " " + fotoShootDate.getMonth().getDisplayName(TextStyle.FULL_STANDALONE ,
              Locale.GERMANY);
      year = fotoShootDate.getYear();
      return TARGET_DIR + "/" + year + "/" + month;
    } else {
      return TARGET_DIR + "/" + "unknown";
    }

  }

  private String monthInTwoDigitNumbers(LocalDate fotoShootDate) {
    int month = fotoShootDate.getMonth().ordinal() + 1;
    String prefix = "";
    if (month < 10) {
      prefix = "0";
    }
    return prefix + month;
  }

  private File toFile(String fotoFileName) {
    File fotoFile = new File(SOURCE_DIR + "/" + fotoFileName);
    if (!fotoFile.exists()) {
      log.error("+++ file does not exist:" + fotoFile.getAbsolutePath());
    }
    return fotoFile;
  }


  private LocalDate extractFotoShootDate(File fotoFile) throws IOException, ImageReadException {
    // get all metadata stored in EXIF format (ie. from JPEG or TIFF).
    ImageMetadata metadata = null;
    try {
      metadata = Imaging.getMetadata(fotoFile);
      if (metadata instanceof JpegImageMetadata) {
        final JpegImageMetadata jpegMetadata = (JpegImageMetadata) metadata;

        // Jpeg EXIF metadata is stored in a TIFF-based directory structure
        // and is identified with TIFF tags.

        log.info("file: " + fotoFile.getPath());
        TagInfo tagInfo = TiffTagConstants.TIFF_TAG_DATE_TIME;
        final TiffField field = jpegMetadata.findEXIFValueWithExactMatch(tagInfo);
        LocalDate localDate = convertFromMetaData((String) field.getValue());
        if (localDate != null) {
          return localDate;
        }
      }
    } catch (Exception e) {
      log.debug("could not process fotoFile: " + fotoFile.getAbsolutePath());
    }
    return tryToGetFromFileCreationDate(fotoFile);

  }

  private LocalDate tryToGetFromFileCreationDate(File fotoFile) {
    try {
      BasicFileAttributes attr = Files.readAttributes(fotoFile.toPath(), BasicFileAttributes.class);
      FileTime fileTime = attr.lastModifiedTime();
      return convertFromFileSystemDate(fileTime.toString());
    } catch (IOException ex) {
      log.error("could not read Filesystem creation date for: " + fotoFile.getAbsolutePath(), ex);
      return null;
    }
  }

  public LocalDate convertFromFileSystemDate(String text) {
    LocalDate fileCreationDate = null;
    String patternWithSecs = "yyyy-MM-dd'T'HH:mm:ss'Z'";

    try {
      fileCreationDate = getLocalDate(patternWithSecs, text);
    } catch (Exception e) {
      log.info("could not parse using pattern with seconds, now trying with ns: " + text);
      fileCreationDate=DEFAULT_ERROR_DATE;
    }
    return fileCreationDate;
  }

  private LocalDate getLocalDate(String pattern, String text) {
    LocalDate fileCreationDate;
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
    fileCreationDate = LocalDate.parse(text, formatter);
    return fileCreationDate;
  }

  public LocalDate convertFromMetaData(String text) {
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy:MM:dd HH:mm:ss");
    return LocalDate.parse(text, formatter);
  }
}