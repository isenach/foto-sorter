package de.baierfamily.foto;

import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.io.filefilter.FileFileFilter;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;

import java.io.File;

import static de.baierfamily.foto.FotoProcessor.SOURCE_DIR;
import static de.baierfamily.foto.FotoProcessor.TARGET_DIR;

@Log4j2
public class ImageCopyBatchListener implements JobExecutionListener {

  public int beforeBatchStartCount = 0;

  public void beforeJob(JobExecution jobExecution) {
    log.info("Called beforeJob().");
    File targetRootDir = new File(TARGET_DIR);
    if (!targetRootDir.exists()) {
      targetRootDir.mkdir();
    }
    beforeBatchStartCount = FileUtils.listFiles(targetRootDir, FileFileFilter.FILE, DirectoryFileFilter.DIRECTORY).size();

  }

  public void afterJob(JobExecution jobExecution) {
    if (jobExecution.getStatus() == BatchStatus.COMPLETED) {
      log.info("Job completed successfully");

      int srcCount = FileUtils.listFiles(new File(SOURCE_DIR), FileFileFilter.FILE, DirectoryFileFilter.DIRECTORY).size();
      int targetCount = FileUtils.listFiles(new File(TARGET_DIR), FileFileFilter.FILE, DirectoryFileFilter.DIRECTORY).size();

      log.info("+++ Target # before start : " + beforeBatchStartCount);
      log.info("+++ Source-Number of files: " + srcCount);
      log.info("+++ Target-Number of files: " + targetCount);
      log.info("+++ detected delta        : " + (targetCount - beforeBatchStartCount-srcCount));
    } else if (jobExecution.getStatus() == BatchStatus.FAILED) {
      log.error("failed to finish job: " + jobExecution.getAllFailureExceptions());
    }
  }
}